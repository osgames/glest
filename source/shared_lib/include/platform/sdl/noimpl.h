#ifndef _NOIMPL_H_
#define _NOIMPL_H_

#define NOIMPL std::cerr << __PRETTY_FUNCTION__ << " not implemented.\n";

#endif

